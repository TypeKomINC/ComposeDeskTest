import androidx.compose.desktop.Window
import androidx.compose.desktop.WindowEvents
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.*
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.IntSize
import homepage.homepage
import org.jetbrains.skija.Bitmap
import java.awt.image.BufferedImage
import java.awt.image.ImageProducer

/**
 * 窗口显示的名称
 * */
const val APP_NAME = "ComposeDesktopTestApp"

/**
 * 窗口大小
 * */
const val INIT_WIDTH = 480
const val INIT_HEIGHT = 600
val INIT_SIZE = IntSize(INIT_WIDTH, INIT_HEIGHT)

/**
 * 程序入口
 * */
fun main() = Window(title = APP_NAME, size = INIT_SIZE, events = windowEvents(), icon = loadIcon(), content = {
  theme()
})

fun loadIcon(): BufferedImage?{
  return null
}

/**
 * 为定制主题的预留
 * */
@Composable
fun theme() {
  return MaterialTheme(
    content = {
      Column(content = {
        Row(horizontalArrangement = Arrangement.Center, content = {
          appBar()
        })
        Row(content = {
          homepage()
        })
      })
    }
  )
}

@Composable
fun appBar() {
  return TopAppBar(title = { Text("Homepage") })
}

/**
 * 窗口事件监听
 * */
fun windowEvents(): WindowEvents {
  return WindowEvents(
    onOpen = {
      println(WindowEvents::class.simpleName + "::" + WindowEvents::onOpen.name)
    },
    onResize = {
      println(WindowEvents::class.simpleName + "::" + WindowEvents::onResize.name)
    },
  )
}
