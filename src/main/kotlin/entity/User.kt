package entity

import com.google.gson.Gson

class User(var username:String,var password:String) {
  override fun toString(): String {
    return Gson().toJson(this)
  }
}